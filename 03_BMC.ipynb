{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# BMC using SMT solvers\n",
    "\n",
    "We can make use of a SMT solvers to decide whether the path formulas we created on the exercise sheet via strongest postcondition are satisfiable or not. For this we will use pySMT, as it gives a easy way to construct formulas and query different SMT solvers under a common API using python.\n",
    "\n",
    "For more information on pySMT, you can check out the paper and/or accompaning talk slides:  \n",
    "\n",
    "> PySMT: a solver-agnostic library for fast prototyping of SMT-based algorithms,Gario, Marco and Micheli, Andrea,SMT Workshop 2015\n",
    "https://andrea.micheli.website/papers/pysmt.pdf  \n",
    "http://smt2015.csl.sri.com/wp-content/uploads/2015/07/smt2015-slides-gario.pdf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now for getting started, let's look at a simple query. We want to proof that `x>0 && y>0 && x*y<0` is unsatisfiable. We can do this as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pysmt.shortcuts import Symbol,Int,LT,GT,And,is_sat,Times\n",
    "from pysmt.typing import INT\n",
    "\n",
    "x = Symbol(\"x\",INT)  #create a variable of type integer (doesn't wrap around like a bitvector)\n",
    "y = Symbol(\"y\",INT)\n",
    "zero = Int(0)  #create a constant\n",
    "formula = And(GT(x,zero),GT(y,zero),LT(Times(x,y),zero))\n",
    "\n",
    "print(f\"Formula: {formula}\")\n",
    "sat = is_sat(formula)\n",
    "print(f\"is sat? {sat}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we all know that on a real computer, this might actually be satisfiable because integers can wrap around.\n",
    "We can observe this by changing the type of our variables to bitvectors. Let's assume we are on a 32 bit system,\n",
    "so we will use bitvectors of size 32."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pysmt.shortcuts import Symbol, BV, BVSLT, BVSGT, And, is_sat, BVMul, reset_env, get_model\n",
    "from pysmt.typing import BVType\n",
    "reset_env() # otherwise pySMT will complain that we defined our variable twice with different type\n",
    "x = Symbol(\"x\",BVType(32))  #create a variable of bitvector type (wraps around)\n",
    "y = Symbol(\"y\",BVType(32))\n",
    "zero = BV(0,32)  #create a constant\n",
    "formula = And(BVSGT(x,zero),BVSGT(y,zero),BVSLT(BVMul(x,y),zero))\n",
    "\n",
    "print(f\"Formula: {formula}\")\n",
    "sat = is_sat(formula)\n",
    "print(f\"is sat? {sat}\")\n",
    "if sat:\n",
    "        print(\"Model:\")\n",
    "        print(get_model(formula))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now this time around we also used the function `get_model` to tell the solver to show us a satisfying assignment.\n",
    "This is often helpful for understanding what happens. We can encapsulate our solver calls into a method for convenience:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def analyze(formula):\n",
    "    print(f\"Formula: {formula}\")\n",
    "    sat = is_sat(formula)\n",
    "    print(f\"is sat? {sat}\")\n",
    "    if sat:\n",
    "        print(\"Model:\")\n",
    "        print(get_model(formula))\n",
    "analyze(formula)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you see the solver will not always give you the same model, it probably outputted different values for x and y this time.\n",
    "\n",
    "# Quantifiers\n",
    "\n",
    "We can also use quantifiers(but not all solvers support this and the queries usually get harder to solve if you make use of them):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pysmt.shortcuts import Exists, Equals\n",
    "reset_env()\n",
    "\n",
    "def var(name):\n",
    "    return Symbol(name,INT)\n",
    "\n",
    "# exists y'. y' = 0 && x>0 && y=5 && x>10\n",
    "formula = Exists([var(\"y'\")],And(\n",
    "    Equals(var(\"y'\"),Int(0)),\n",
    "    GT(var(\"x\"),Int(0)),\n",
    "    Equals(var(\"y\"),Int(5)),\n",
    "    GT(var(\"x\"),Int(10))\n",
    "))\n",
    "analyze(formula)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# SSA Form\n",
    "\n",
    "We can construct an equisatisfiable formula using SSA form and thus get rid of the existential quantifier:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reset_env()\n",
    "\n",
    "def var(name):\n",
    "    return Symbol(name,INT)\n",
    "\n",
    "def analyze(formula):\n",
    "    print(f\"Formula: {formula}\")\n",
    "    sat = is_sat(formula)\n",
    "    print(f\"is sat? {sat}\")\n",
    "    if sat:\n",
    "        print(\"Model:\")\n",
    "        print(get_model(formula))\n",
    "\n",
    "# y0 = 0 && x0>0 && y1=5 && x0>10\n",
    "formula= And(\n",
    "    Equals(var(\"y0\"),Int(0)),\n",
    "    GT(var(\"x0\"),Int(0)),\n",
    "    Equals(var(\"y1\"),Int(5)),\n",
    "    GT(var(\"x0\"),Int(10))\n",
    ")\n",
    "\n",
    "analyze(formula)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observe that this time the model includes a value for the variable $y_0$ that was existentially quantified before."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Checking our solutions from the exercise sheet\n",
    "\n",
    "## Exercise 1 and 2\n",
    "\n",
    "The examples above are formulas for exercise 1d) and 2d). Can you create and check formulas for the other parts (a)-c)) of that exercise?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#TODO: check your strongest post computations using a SMT solver"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 1a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 1b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 1c)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 2a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 2b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 2c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 3\n",
    "\n",
    "Construct a formula that describes the following path to the error location and proof that it is infeasible:  \n",
    "$l_0 \\rightarrow l_1 \\rightarrow l_2 \\rightarrow l_3 \\rightarrow l_{ERR}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#TODO: proof that the error cannot be reached via the path above"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
